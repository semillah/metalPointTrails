//
//  metalPointTrailsApp.swift
//  metalPointTrails
//
//  Created by Raul on 1/7/24.
//

import SwiftUI

@main
struct metalPointTrailsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
